const express = require("express");
const router = express.Router();

const ListaController = require("../controller/ListaController");
router.route("/agregar_al_inicio").post(ListaController.AgregarAlInicio);
router.route("/agregar_al_final").post(ListaController.AgregarAlFinal);
router.route("/mostrar_lista").post(ListaController.Mostrarlistar); 

module.exports = router;
