const express = require("express");
const router = express.Router();

const PilaController = require("../controller/PilaController");
router.route("/apilar").post(PilaController.Apilar);
router.route("/desapilar").post(PilaController.Desapilar);
router.route("/cima").post(PilaController.Cima);
router.route("/vacia").post(PilaController.Vacia);

module.exports = router;
