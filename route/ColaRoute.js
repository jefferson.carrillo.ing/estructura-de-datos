const express = require("express");
const router = express.Router();

const ColaController = require("../controller/ColaController");
router.route("/encolar").post(ColaController.Encolar);
router.route("/desencolar").post(ColaController.Desencolar);
router.route("/frontal").post(ColaController.Frontal);
router.route("/vacia").post(ColaController.Vacia);

module.exports = router;
