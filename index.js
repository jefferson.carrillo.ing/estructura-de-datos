const express = require("express");
const bodyParser = require("body-parser");

const app = express();
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
const port = 5005;//PUERTO DONDE CORRE EL SERVICIO

//INSTANCIAMOS LAS RUTAS
var PilaRoute = require("./route/PilaRoute");
app.use("/pila", PilaRoute);

var ColaRoute = require("./route/ColaRoute");
app.use("/cola", ColaRoute);

var ListaRoute = require("./route/ListaRoute");
app.use("/lista", ListaRoute);

app.listen(port, () => {
  console.log(`El SERVICIO ESTRUCTURA ESTA CORRIENDO http://localhost:${port}`);
});
