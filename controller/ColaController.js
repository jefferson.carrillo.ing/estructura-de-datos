//TAD COLA SIMPLE
var mi_cola = [];
var primero = 0;
var ultimo = -1;
var numElem = 0;

async function Encolar(req, res, next) {
  let p = req.body;
  let dato = p.dato;
  let msj = "";

  ultimo = ultimo + 1;
  mi_cola[ultimo] = dato;
  numElem++;
  msj = "Dato (" + dato + ") fue encolado;";

  res.jsonp({ msj });
}

async function Desencolar(req, res, next) {
  let msj = "";
  var dato = "";
  if (numElem != 0) {
    dato = mi_cola[primero];
    primero = primero + 1;
    numElem--;
  }

  msj = "Dato (" + dato + ") fue desencolado;";

  res.jsonp({ msj });
}

async function Frontal(req, res, next) {
  let msj = "";
  var dato = "";
  if (numElem != 0) {
    dato = mi_cola[primero];
  }
  msj = "Dato (" + dato + ") al inicio de la cola;";
  res.jsonp({ msj });
}

async function Vacia(req, res, next) {
  let msj = "";

  if (numElem == 0) {
    msj = "Cola esta vacia.";
  } else {
    msj = "Cola no esta vacia.";
  }

  res.jsonp({ msj });
}

module.exports = {
  Encolar,
  Desencolar,
  Frontal,
  Vacia,
};
