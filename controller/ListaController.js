//LISTA SIMPLE

let inicio = null;
let tamanio = 0;

function AgregarAlFinal(req, res, next) {
  let p = req.body;
  let dato = p.dato;

  // Define un nuevo nodo.
  let nuevo = {
    valor: "",
    siguiente: null,
  };
  // Agrega al valor al nodo.
  nuevo.valor = dato;
  // Consulta si la lista esta vacia.
  if (esVacia()) {
    // Inicializa la lista agregando como inicio al nuevo nodo.
    inicio = nuevo;
    // Caso contrario recorre la lista hasta llegar al ultimo nodo
    //y agrega el nuevo.
  } else {
    // Crea ua copia de la lista.
    let aux = inicio;
    // Recorre la lista hasta llegar al ultimo nodo.
    while (aux.siguiente != null) {
      aux = aux.siguiente;
    }
    // Agrega el nuevo nodo al final de la lista.
    aux.siguiente = nuevo;
  }
  // Incrementa el contador de tamaño de la lista
  tamanio++;
  let msj = "Dato (" + dato + ") fue listado al final.";
  res.jsonp({ msj });
}

function AgregarAlInicio(req, res, next) {
  let p = req.body;
  let dato = p.dato;
  // Define un nuevo nodo.
  let nuevo = {
    valor: "",
    siguiente: null,
  };
  // Agrega al valor al nodo.
  nuevo.valor = dato;
  // Consulta si la lista esta vacia.
  if (esVacia()) {
    // Inicializa la lista agregando como inicio al nuevo nodo.
    inicio = nuevo;
    // Caso contrario va agregando los nodos al inicio de la lista.
  } else {
    // Une el nuevo nodo con la lista existente.
    nuevo.siguiente = inicio;
    // Renombra al nuevo nodo como el inicio de la lista.
    inicio = nuevo;
  }
  // Incrementa el contador de tamaño de la lista.
  tamanio++;
  let msj = "Dato (" + dato + ") fue listado al inicio.";
  res.jsonp({ msj });
}

function Mostrarlistar(req, res, next) {
  // Verifica si la lista contiene elementoa.
  let lista = "";
  if (!esVacia()) {
    // Crea una copia de la lista.
    let aux = inicio;
    // Posicion de los elementos de la lista.
    let i = 0;
    // Recorre la lista hasta el final.
    while (aux != null) {
      // Imprime en pantalla el valor del nodo.
      lista = lista + "[" + i + ": " + aux.valor + " ]-> ";
      // Avanza al siguiente nodo.
      aux = aux.siguiente;
      // Incrementa el contador de la posión.
      i++;
    }
  }

  let msj = lista;
  res.jsonp({ msj });
}

function esVacia() {
  return inicio == null;
}
module.exports = {
  AgregarAlInicio,
  AgregarAlFinal,
  Mostrarlistar,
};


//EJEMPLO EN JAVA
//http://codigolibre.weebly.com/blog/listas-simples-en-java