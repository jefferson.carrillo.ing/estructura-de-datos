//TAD PILA
var mi_pila = []; //INICIALIZA LA PILA
var cima = 0;

//Apilar: Inserta un elemento en la pila.
async function Apilar(req, res, next) {
  let p = req.body;
  let dato = p.dato;
  let msj = "";
  mi_pila.push(dato);
  cima = mi_pila.length;
  msj = "Dato (" + dato + ") fue apilado; cima actual : (" + cima + ").";
  res.jsonp({ msj });
}

//Desapilar: Extrae el elemento situado en la cima de la pila.
async function Desapilar(req, res, next) {
  let msj = "";
  let dato = mi_pila[cima - 1];
  delete mi_pila.splice(cima - 1, 1);
  cima = mi_pila.length;
  msj = "Dato (" + dato + ") fue desapilado; cima actual: (" + cima + ").";
  res.jsonp({ msj });
}

//Cima: También llamada encima ,Devuelve el elemento situado en la cima de la pila, sin extraerlo de ella.

async function Cima(req, res, next) {
  let msj = "";
  cima = mi_pila.length;
  let dato = mi_pila[cima - 1];
  msj = "Dato (" + dato + ") esta en la cima : (" + cima + ").";
  res.jsonp({ msj });
}

//Vacía: Devuelve cierto si la pila esta vacía y falso en caso contrario.
async function Vacia(req, res, next) {
  let msj = "";
  mi_pila.length;
  if (cima == 0) {
    msj = "Pila esta vacia.";
  } else {
    msj = "Pila no esta vacia.";
  }

  res.jsonp({ msj });
}

module.exports = {
  Apilar,
  Desapilar,
  Cima,
  Vacia,
};
